FROM node:10.13-alpine
ENV NODE_ENV = $NODE_ENV
ENV DB_E_URI = $DB_E_URI
WORKDIR /usr/src/app
COPY ["package.json", "package-lock.json*", "npm-shrinkwrap.json*", "./"]
RUN yarn install --silent && mv node_modules ../
COPY . .
EXPOSE 3000