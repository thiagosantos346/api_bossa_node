FROM node:latest
ENV NODE_ENV = $NODE_ENV
ENV DB_E_URI = $DB_E_URI
ENV PORT_LISTENING = $PORT_LISTENING
WORKDIR /usr/src/app
COPY ["package.json", "package-lock.json*", "npm-shrinkwrap.json*", "./"]
RUN yarn install && mv node_modules ../
COPY . .
EXPOSE 3000