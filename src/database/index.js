const mongoose = require('mongoose');
const consts = require('./consts');
const config = require('./config');

class DatabaseConnector{
    constructor(){
        this.mongoose = mongoose;
        this.mongoose.Promise = Promise;
        this.mongoose.connect = mongoose.connect(consts.uri, config);
    }

    disconnect(done){
        this.mongoose.disconnect(done);
    }
}


module.exports = new DatabaseConnector();