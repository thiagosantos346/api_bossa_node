module.exports = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    autoReconnect : true,
    socketTimeoutMS: 9000,
    keepAlive : false,
}