const process = require('process');
const request = require('supertest');
const app = require ('../app');
const Tool = require('../app/models/Tool');
const consts = require('./consts');

let server;


describe('This test the root path', () => {
    beforeEach(async (done)=>{
        server = app.listen(4000, (err)=>{
            if(err) return done(err);
            global.agent = request.agent(server);
            done();
        });

        await Tool.find( (err, docs)=>{
          if(!err){
            docs.map( async (tool)=>{
              if(tool._id){
                await Tool.deleteMany({ "_id" : tool._id.toString() }); 
              }
            });
          }
        });
    });
    
    afterEach(async  (done)=>{
      await server && server.close(done);
    });


    afterAll(async ()=>{ 
      await Tool.db.close()
    });

    it('Should return status 200 when a query in the root path.', async () => {
      const { title, link, description, tags } = consts[0];
      try {
        const tool = await Tool.create({ title, link, description, tags});
        await tool.save();
        const response = await global.agent.get('/tools');
        expect(response.status).toBe(200);
      } catch (error) {
        console.error(error) 
      }
    });

    it('Should return status 200 when a query a tool by id.', async () => {
      const { title, link, description, tags } = consts[0];
      try{
        const tool = await Tool.create({ title, link, description, tags });
        await tool.save();
        const response  = await global.agent.get(`/tools/${tool._id}`).send();
        expect(response.status).toBe(200);
      }catch(error){
        console.error(error);
      }
    });

    
    it('Should return status 200 when a query a tool by tag.', async () => {
      const dataRequest = { title, link, description, tags } = consts[0];
      const tool = await Tool.create(dataRequest);
      await tool.save();
      const response  = await global.agent.get(`/tools?tag=${tool.tags[0]}`).send();
      const dataResponse = { title, link, description, tags } =  response.body[0];
      expect(response.status).toBe(200);
      expect(dataRequest.tags[0]).toBe(dataResponse.tags[0]);
    });

    it('Should return status 404, when tool no exists.', async () => {
      const { title, link, description, tags } = consts[0];
      let id = (-1);
      id = id.toString();
      const response  = await global.agent.get(`/tools/${id}`).send();
      expect(response.status).toBe(404);
      
    });

    it('Should return status 201, when post one tool register', async ()=>{

      const { title, link, description, tags } = consts[0];
      const response  = await global.agent.post('/tools').send({ title, link, description, tags });
      expect(response.status).toBe(201);

    });

    it('Should return status 204, when has delete one tool register.', async ()=>{
      
      const { title, link, description, tags } = consts[0];
      const tool = await Tool.create({ title, link, description, tags });
      await tool.save();
      const response  = await global.agent.delete(`/tools/${tool._id}`).send();
      expect(response.status).toBe(204);
      
    });

    it('Should return status 404, when tool to delete not exists', async ()=>{
      let id = (-1);
      id = id.toString();
      const response  = await global.agent.delete(`/tools/${id}`).send();
      expect(response.status).toBe(404);
  });

});