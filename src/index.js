const app = require('./app')
const PORT = process.env.PORT_LISTENING || 3000
const WELCOME_MESSAGE = `Ouvindo a porta ${PORT}...`;

app.listen(PORT, ()=>{ console.info(WELCOME_MESSAGE)});