const express = require('express');
const routes = require('./controllers/index');

class AppController {

    constructor(){
        this.express = express();
        this.middlewares();
        this.routers();
    }

    middlewares(){
        this.express.use(express.json());
    }

    routers(){
        this.express.use(routes);
    }

}

module.exports = new AppController().express;