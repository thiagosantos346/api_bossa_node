const database = require('../../database');
const autoIncrement = require('mongoose-auto-increment');

class Tool{
    constructor(){
        this.connect = database.mongoose;
        
        autoIncrement.initialize(this.connect);

        this.ToolSchema = new database.mongoose.Schema({
            title : {
                type: String,
                require: true,
            },
            link : {
                type: String,
                require: true,
            },
            description : {
                type : String,
                require : true,
            },
            tags : [{
                type: String,
            }],
            owner : {
                type : database.mongoose.Schema.Types.ObjectId,
                ref: 'User',
                require: true
            },
            createdAt : {
                type: Date,
                defauld: Date.now,
            }
            
        });

        this.ToolSchema.plugin(autoIncrement.plugin, 'Tool');
    }

}

module.exports = database.mongoose.model('Tool', new Tool().ToolSchema);