//const authMiddleware = require('../middleware/auth');

//routers.use(authMiddleware);
const Tool = require('../models/Tool');

class ToolController{
    
    async getAll(req, res){
        try {
            const tools = await Tool.find();
                                //.populate(['owner']);

            return res.status(200).send(tools);
        } catch (error) {
            console.log(error)
            return res.status(400).send( error );
        }
    }
    
    async getOne(req, res){
        try {
            const tools = await Tool
                            .findById(req.params.toolId);
                            //.populate(['owner']);
            if( tools !== null){
                return res.status(200).send(tools);   
            }
    
            return res.status(404).send();
    
        } catch (error) {
            return res.status(400).send({"error:":error});
        }
    }
    
    async post(req, res){
        try{
            const { title, link, description, tags } = req.body;
            const tool = await Tool.create({ title, link, description, tags, owner : req.userId });

            await tool.save();
            return res.status(201).send({tool});
    
        } catch( erro ){
            return res.status(400).send({ "error" : error })
        }
    }
    
    async put(req, res){ 
        try{
            const { title, link, description, tags } = req.body;
            const tool = await Tool.findByIdAndUpdate(req.query.toolId, {
                title, 
                link,
                description
            }, { new : true });
    
            if(tool === null){
                return res.status(404).send({'Not Found': req.query });
            }
            
            tool.tags = [];
    
            await Tag.deleteOne({ tool : tool._id });
    
            await Promise.all ( tags.map(async tags =>{
                const toolTags = new Tag({...tags, tool : tool._id});
                await toolTags.save();
                tool.tags.push(toolTags);
    
            }));
    
            await tool.save();
            return res.status(201).send({ tool })
    
        } catch ( error ){
            return res.status(400).send({ "error" : erreor});
        }
    }
    
    async delete(req, res){ 
        try{
            
            const tools = await Tool.findByIdAndRemove(req.params.toolId);
            let vStatus = (tools !== null ) ? 204 : 404;
            return res.status(vStatus).send();
        
        } catch ( error ){
            return res.status(400).send({ "error" : error});
        }
    }


}

module.exports = new ToolController;