const routers = require('express').Router();
const ToolController = require('./toolController');

routers.get("/tools", ToolController.getAll);
routers.get("/tools/:toolId", ToolController.getOne);
routers.post("/tools", ToolController.post);
routers.put("/tools", ToolController.put);
routers.delete("/tools/:toolId", ToolController.delete);

module.exports = routers;