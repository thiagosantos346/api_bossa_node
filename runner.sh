sudo docker container prune
sudo docker run -d --name gitlab-runner --restart always \
  -v /srv/gitlab-runner/config:/etc/gitlab-runner \
  -v /var/run/docker.sock:/var/run/docker.sock \
  gitlab/gitlab-runner:latest
sudo docker exec -it gitlab-runner bash
gitlab-runner register
https://gitlab.com/